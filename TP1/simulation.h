#ifndef SIMULATION_H
#define SIMULATION_H

typedef struct Piece {
	int numPiece;
	int dateEntreeSys;
	int dateSortieSys;
	int dureeTraitement;
} T_Piece;

enum Etat {
	Plein,
	Vide,
	Bloque
};

typedef struct Entree {
	enum Etat etat;
	int dateProchaineEvt;
	T_Piece contenu;
	int compteur;
} T_Entree;

typedef struct Serveur {
	enum Etat etat;
	int dateProchaineEvt;
	T_Piece contenu;
} T_Serveur;

typedef struct Sortie {
	T_Piece* tab;
	int nbElements;
	int taille;
} T_Sortie;


typedef struct Element
{
    T_Piece piece;
} Element_t;


#endif