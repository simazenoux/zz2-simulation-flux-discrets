#include <iostream>
#include <queue>
#include "simulation.h"


const int TAILLE = 8;

using namespace std;

void simuler(int dureeSimulationMax, int dureeEntreDeuxClients, int dureeTraitementClient){
	
	// Initialisation
	queue<T_Piece> myqueue;


	T_Entree entree;
	T_Serveur serveur;
	T_Sortie sortie;

	int dateCourante = 0;

	entree.etat = Vide;
	entree.dateProchaineEvt = 0;
	entree.compteur = 0;

	serveur.etat = Vide;
	serveur.dateProchaineEvt = INT32_MAX;
	
	sortie.taille = dateCourante / dureeEntreDeuxClients;
	sortie.tab = (T_Piece *) malloc(sortie.taille * sizeof(T_Piece));
	sortie.nbElements = 0;


	// Déroulement
	while (dateCourante < dureeSimulationMax)
	{
		if (entree.dateProchaineEvt <= serveur.dateProchaineEvt)
		{

			dateCourante = entree.dateProchaineEvt;
			T_Piece piece;
			piece.dateEntreeSys = dateCourante;
			piece.dureeTraitement = dureeTraitementClient;
			piece._numero = entree.compteur;
			entree.contenu = piece;
			entree.compteur++;

			if (myqueue.size() == TAILLE)
			{
				entree.etat = Bloque;
				entree.dateProchaineEvt = INT32_MAX;
			}
			else
			{
				if (serveur.etat == Vide)
				{
					serveur.contenu = piece;
					serveur.etat = Plein;
					entree.etat = Vide;
				}
				else 
				{
                    myqueue.push(piece);
				}
				entree.dateProchaineEvt = dateCourante + dureeEntreDeuxClients;
			}
		}
		else
		{
			dateCourante = serveur.dateProchaineEvt;
			sortie.tab[sortie.nbElements] = serveur.contenu;
			sortie.nbElements++;
            cout << sortie.tab[sortie.nbElements].dureeTraitement << '\t';
			serveur.etat = Vide;
			serveur.dateProchaineEvt = INT32_MAX;

			if (myqueue.size() != 0) // Si la file est non vide
			{
				serveur.contenu = myqueue.front();
				serveur.etat = Plein;
				serveur.dateProchaineEvt = dateCourante + dureeTraitementClient;
				myqueue.pop();
                if (entree.etat == Bloque)
				{
                    myqueue.push(entree.contenu);
					entree.etat = Vide;
					entree.dateProchaineEvt = dateCourante;
				}
			}
		}
	}
	
}

int main()
{
    simuler(100,10,2);
}