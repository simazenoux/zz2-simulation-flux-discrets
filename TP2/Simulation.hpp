#ifndef SIMULATION_HPP
#define SIMULATION_HPP

#include <iostream>
#include <vector>

#include "Machine.hpp"
#include "Entree.hpp"
#include "Output.hpp"
#include "Assembleur.hpp"

using namespace std;


class Simulation
{
    private:
        double _dateCourante;
        vector<Element*> _activeElements;
        vector<Element*> _outputs;
        
    public:
        Simulation();

        void addActiveElement(Element* e);
        void addOutput(Output* o);

        Element* getActiveElementWithLowestDpe();

        void simuler(double dureeSimulation);
        void display();
};

#endif