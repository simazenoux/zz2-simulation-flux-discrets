#include "Machine.hpp"


Machine::Machine(string name, double dureeTraitement, int capacity): Element(name, INFINITY, dureeTraitement, Waiting), _capacity(capacity)
{
    _pieces = queue<Piece*>();
}


void Machine::insert(double dateCourante)
{
    Element* previous = selectPreviousElement();
    Piece* p = previous->removePiece(dateCourante);
    _pieces.push(p);

    setState(Working);
    setDpe(dateCourante + this->getDureeTraitement());
}


Piece* Machine::removePiece(double dateCourante)
{
    Piece* piece = _pieces.front();
    _pieces.pop();

    if (_pieces.size() == 0)
    {
        setState(Waiting);
        setDpe(INFINITY);
    }
    else
    {
        setState(Working);
        setDpe(dateCourante + getDureeTraitement());
    }

    return piece;
}



bool Machine::isEmpty()
{
    return _pieces.size()==0;
}

bool Machine::isFull()
{
    return _pieces.size() == _capacity;
}

bool Machine::isReady()
{
    return !isFull() && getState() != Blocked;
}


void Machine::display()
{
    Element::display();


    if (_pieces.size() == 0)
    {
        cout << "Aucune piece dans la machine" << endl;
    }
    else
    {
        cout << "Pieces encore dans la machine :" << endl;

        queue<Piece *> tmp_q = _pieces; //copy the original queue to the temporary queue

        while (!tmp_q.empty())
        {
            tmp_q.front()->display();
            tmp_q.pop();
        }
    }

    cout << endl << endl;

}