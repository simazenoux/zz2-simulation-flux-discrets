#include "Simulation.hpp"

Simulation* genererTestAleatoire()
{
     Simulation * simulation = new Simulation();


    Element* entree = new Entree("Entree", 1);

    int capacity = 100;

    Element* m1 = new Machine("Machine 1", 10.0, capacity);
    Element* m2 = new Machine("Machine 2", 5.0, capacity);
    Element* m3 = new Machine("Machine 3", 5.0, capacity);

    Output* sortie = new Output("Sortie");


    entree->addNextElement(m1, 1.0);
    m1->addNextElement(m2, 0.5);
    m1->addNextElement(m3, 0.5);
    m2->addNextElement(sortie, 1.0);
    m3->addNextElement(sortie, 1.0);


    simulation->addActiveElement(entree);
    simulation->addActiveElement(m1);
    simulation->addActiveElement(m2);
    simulation->addActiveElement(m3);

    for (int i=0; i<10; i++)
    {
        m1->selectNextElement()->display();
    }
    

    simulation->addOutput(sortie);

    return simulation;
}

Simulation* genererSimulation1(int Lam = 10.0, double p = 0.6, double q=0.4, double Sa=1.0, double Sb=0.8, double Sc=0.6)
{
    Simulation * simulation = new Simulation();


    Element* entree = new Entree("Entree", Lam);

    int capacity = 10;

    Element* m1 = new Machine("Machine 1",Sa, capacity);
    Element* m2 = new Machine("Machine 2", Sb, capacity);
    Element* m3 = new Machine("Machine 3", Sc, capacity);

    Output* sortie = new Output("Sortie");


    entree->addNextElement(m1, 1.0);
    m1->addNextElement(m2, q);
    m1->addNextElement(m3, 1-q);
    m2->addNextElement(sortie, p);
    m2->addNextElement(m1, 1-p);
    m3->addNextElement(sortie, p);
    m3->addNextElement(m1, 1-p);


    simulation->addActiveElement(entree);
    simulation->addActiveElement(m1);
    simulation->addActiveElement(m2);
    simulation->addActiveElement(m3);

    simulation->addActiveElement(sortie);

    return simulation;
}



Simulation* genererSimulation2(double DE1=1.0, double DE2=10.0, double DT1=5.0, double DT2=1.0, double DASS = 10.0, int capacite = 8)
{
    Simulation * simulation = new Simulation();


    Element* entree1 = new Entree("Entree 1", DE1);
    Element* entree2 = new Entree("Entree 2", DE2);
    Element* m1 = new Machine("Machine 1", DT1, capacite);
    Element* m2 = new Machine("Machine 2", DT2, capacite);

    Element* assembleur = new Assembleur("Assembleur", DASS);

    Element* sortie = new Output("Sortie");


    entree1->addNextElement(m1, 1.0);
    entree2->addNextElement(m2, 1.0);
    m1->addNextElement(assembleur, 1.0);
    m2->addNextElement(assembleur, 1.0);
    assembleur->addNextElement(sortie, 1.0);


    simulation->addActiveElement(entree1);
    simulation->addActiveElement(entree2);
    simulation->addActiveElement(m1);
    simulation->addActiveElement(m2);
    simulation->addActiveElement(assembleur);

    simulation->addActiveElement(sortie);

    return simulation;
}


int main(int, char **)
{
    Simulation * simulation;

    // simulation = genererTestAleatoire();
    simulation = genererSimulation1();
    // simulation = genererSimulation2();

    double dureeSimulation = 1000;

    cout << "Début simulation" << endl;
    simulation->simuler(dureeSimulation);
    cout << "Fin simulaiton" << endl << endl;

    simulation->display();

    delete simulation;
    
    return 0;
}