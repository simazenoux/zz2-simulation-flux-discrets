#include "Piece.hpp"

Piece::Piece(double dateCourante): _numero(_nombrePiecesEmises++), _dateEntree(dateCourante)
{}

void Piece::setDateSortieSys(double dateCourante)
{
    _dateSortie = dateCourante;
    _dureeTraitement = dateCourante - _dateEntree;
}

int Piece::getNumero()
{
    return _numero;
}

double Piece::getDateEntree()
{
    return _dateEntree;
}

double Piece::getDateSortie()
{
    return _dateEntree;
}


void Piece::setDateSortie(double dateSortie)
{
    _dateSortie = dateSortie;
}

void Piece::display()
{
    cout << "Pièce n°" << _numero << ", entrée = " << _dateEntree << ", sortie = " << _dateSortie << endl;
}

