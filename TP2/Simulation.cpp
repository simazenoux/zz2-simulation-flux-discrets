#include "Simulation.hpp"


Simulation::Simulation(): _dateCourante(0.0)
{}



void Simulation::addActiveElement(Element* e)
{
    _activeElements.push_back(e);
}


void Simulation::addOutput(Output* o)
{
    _outputs.push_back(o);
}


Element* Simulation::getActiveElementWithLowestDpe()
{
	Element* a;
	double lowestDpe = INFINITY;
	for (std::vector<Element*>::iterator it = _activeElements.begin() ; it != _activeElements.end(); ++it)
	{
		if ((*it)->getDpe() < lowestDpe)
		{
			lowestDpe = (*it)->getDpe();
            a = (*it);
		}
	}
    return a;
}


void Simulation::simuler(double dureeSimulation)
{
    while (_dateCourante < dureeSimulation)
    {
        Element* current = getActiveElementWithLowestDpe();
        _dateCourante = current->getDpe();

        // Affichage
        cout << _dateCourante << " : " << current->getName() << endl;

        Element* next = current->selectNextElement();
        next->addWaitingElement(current);

        current->setState(Blocked);
        

        if (next->isReady())
        {
            next->insert(_dateCourante);
        }
    }
}


void Simulation::display()
{
    cout << "SIMULATION --------------------------------------" << endl;
    cout << "Date courante : " << _dateCourante << endl << endl;

    cout << "Elements : " << endl;
    for (std::vector<Element*>::iterator it = _activeElements.begin() ; it != _activeElements.end(); ++it)
	{
        (*it)->display();
        cout << endl;
	}
    cout << endl;
}

