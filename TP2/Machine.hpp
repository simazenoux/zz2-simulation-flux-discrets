#ifndef MACHINE_HPP
#define MACHINE_HPP


#include "Piece.hpp"
#include "Element.hpp"

using namespace std;

class Machine : public Element
{
    private:
        queue<Piece*> _pieces;
        int _capacity;
        bool isEmpty();
        bool isFull();

    public:
        Machine(string name, double dureeTraitement, int capacity);

        virtual void display();
        virtual bool isReady();
        virtual void insert(double dateCourante);
        virtual Piece* removePiece(double dateCourante);
};

#endif