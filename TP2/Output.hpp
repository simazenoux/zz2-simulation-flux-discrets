#ifndef OUTPUT_HPP
#define OUTPUT_HPP

#include <queue>

#include "Element.hpp"

using namespace std;
class Output : public Element
{
    private:
        queue<Piece*> _pieces;

    public:
        Output(string name);
        virtual void display();
        virtual bool isReady();
        virtual void insert(double date);
        virtual Piece* removePiece(double dateCourante);
};

#endif