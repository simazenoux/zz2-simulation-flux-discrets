#ifndef PIECE_COMPOSEE_HPP
#define PIECE_COMPOSEE_HPP

#include <iostream>
#include <vector>

#include "Piece.hpp"


class PieceComposee : public Piece
{
    private:
        vector<Piece*> _pieces;

    public:
        PieceComposee(double dateCourante);
        void addPiece(Piece* piece);
        virtual void display();
};
#endif