#include "Entree.hpp"

Entree::Entree(string name, double dureeTraitement): Element(name, 0, dureeTraitement, Working)
{}

Piece* Entree::removePiece(double dateCourante) 
{
    insert(dateCourante);
    Piece* piece = _piece;
    _piece = NULL;

    setState(Working);
    setDpe(dateCourante + getDureeTraitement());

    return piece;
}

bool Entree::isReady()
{
    return getState() == Waiting;
}


void Entree::insert(double dateCourante)
{
    _piece = new Piece(dateCourante);
}



void Entree::display()
{
    Element::display();

    if (_piece)
    {
        cout << "Piece en attente : " << endl;
        _piece->display();
    }
    else
    {
        cout << "Aucune pièce dans la machinie" << endl;
    }
    cout << endl << endl;
}