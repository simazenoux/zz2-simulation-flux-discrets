#ifndef ENTREE_HPP
#define ENTREE_HPP

#include "Element.hpp"

class Entree : public Element
{
    private:
        Piece* _piece;

    public:
        Entree(string name, double dureeTraitement);

        virtual bool isReady();
        virtual void display();
        virtual void insert(double dateCourante);
        virtual Piece* removePiece(double dateCourante);
};

#endif

