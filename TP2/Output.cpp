#include "Output.hpp"

Output::Output(string name): Element(name, INFINITY, 0, Waiting)
{}

void Output::display()
{
    cout << "Name : " << getName() << endl;
    

    if (_pieces.empty())
    {
        cout << "Aucune pièce dans la sortie" << endl;
    }
    else
    {
        cout << "Pieces : " << endl;
        queue<Piece *> tmp_q = _pieces; //copy the original queue to the temporary queue

        while (!tmp_q.empty())
        {
            cout << "\t";
            tmp_q.front()->display();
            tmp_q.pop();
        }
    }
}


bool Output::isReady()
{
    return true;
}

void Output::insert(double dateCourante)
{
    Element* previous = selectPreviousElement();
    Piece* p =  previous->removePiece(dateCourante);
    p->setDateSortieSys(dateCourante);
    _pieces.push(p);
}

Piece* Output::removePiece(double dateCourante)
{
    Piece* p = _pieces.front();
    _pieces.pop();
    
    return p;
}