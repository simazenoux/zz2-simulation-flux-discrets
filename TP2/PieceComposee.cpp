#include "PieceComposee.hpp"

PieceComposee::PieceComposee(double dateCourante): Piece(dateCourante)
{
    _pieces = vector<Piece*>();
}

void PieceComposee::addPiece(Piece* piece)
{
    piece->setDateSortie(_dateEntree);
    _pieces.push_back(piece);
}

void PieceComposee::display()
{
    Piece::display();

    cout << "Sous-pièces : " << endl;
    
    for (auto piece : _pieces) // access by reference to avoid copying
    {  
        cout << "\t";
        piece->display();
    }
}