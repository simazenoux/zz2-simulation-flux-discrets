#include "Element.hpp"


Element::Element(string name, double dpe, double dureeTraitement, State state): _name(name),  _dpe(dpe) , _dureeTraitement(dureeTraitement), _state(state)
{}

string Element::getName()
{
    return _name;
}

double Element::getDpe()
{
    return _dpe;
}

double Element::getDureeTraitement()
{
    return _dureeTraitement;
}

State Element::getState()
{
    return _state;
}

void Element::setState(State state)
{
    _state = state;
}

void Element::setDpe(double dpe)
{
    _dpe = dpe;
}

void Element::display()
{
    cout << "Name : " << getName() << endl;
    cout << "Duree traitement : " << getDureeTraitement() << endl;
    cout << "State : " << getState() << endl;
    cout << "Dpe : " << getDpe() << endl;
    cout << "Elements suivants : " << endl;
    for (std::vector<tuple<Element*,double>>::iterator it = _nextElements.begin() ; it != _nextElements.end(); ++it)
	{
        cout << "\tName : " << get<0>(*it)->getName() << ", proba = " << get<1>(*it) << endl;
	}
}

void Element::addNextElement(Element* e, double p)
{
    tuple<Element*, double> t (e, p);
    _nextElements.push_back(t);
}

Element* Element::selectPreviousElement()
{
    Element* a = _waitingElements.front();
    _waitingElements.pop();
    return a;
}


Element* Element::selectNextElement()
{
    std::random_device rd;
    std::uniform_real_distribution<double> unif(0,1);
    std::default_random_engine re(rd());

    double sommeProbas = 0;
    double p = unif(re);

    int i=0;
    while (i< _nextElements.size() && get<1>(_nextElements[i]) + sommeProbas < p)
    {
        sommeProbas += get<1>(_nextElements[i]);
        i++;
    }

    return get<0>(_nextElements[i]);
}


void Element::addWaitingElement(Element* m)
{
    this->_waitingElements.push(m);
}