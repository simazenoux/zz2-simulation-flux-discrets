#ifndef PIECE_HPP
#define PIECE_HPP

#include <iostream>

using namespace std;

static int _nombrePiecesEmises = 0;

class Piece
{
    
    
    protected:
        int _numero;
        double _dateEntree;
        double _dateSortie;
        double _dureeTraitement;
        

    public:
        Piece(double dateCourante);
        void setDateSortieSys(double dateCourante);
        int getNumero();
        double getDateEntree();
        double getDateSortie();
        void setDateSortie(double dateSortie);
        void display();
};

#endif