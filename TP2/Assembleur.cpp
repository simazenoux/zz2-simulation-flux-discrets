#include "Assembleur.hpp"

Assembleur::Assembleur(string name, double dureeTraitement): Element(name, INFINITY, dureeTraitement, Waiting)
{
    _previousActiveElements = vector<Element*>();
}


void Assembleur::addPreviousActiveElement(Element* activeElement)
{
    _previousActiveElements.push_back(activeElement);
}


bool Assembleur::isReady()
{
    bool isReady = true;
    int index = 0;
    while ( index < _previousActiveElements.size() && isReady) {
        if (_previousActiveElements.at(index)->getState() != Blocked)
        {
            isReady = false;
        }
    }
    return isReady;
}

void Assembleur::insert(double dateCourante)
{
    PieceComposee* pieceComposee = new PieceComposee(dateCourante);

    for (auto &previousActiveElement : _previousActiveElements) // access by reference to avoid copying
    {  
        pieceComposee->addPiece(previousActiveElement->removePiece(dateCourante));
    }

    setState(Working);
    setDpe(dateCourante + getDureeTraitement());
}

Piece* Assembleur::removePiece(double dateCourante)
{
    Piece* piece = _piece;
    
    _piece = NULL;

    // Si une autre piece peut être immédiatement réalisée
    if (isReady())
    {
        insert(dateCourante);
    }
    else
    {
        setState(Waiting);
        setDpe(INFINITY);
    }
    return piece;
}
