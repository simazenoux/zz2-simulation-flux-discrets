#ifndef ASSEMBLEUR_HPP
#define ASSEMBLEUR_HPP

#include <iostream>
#include <vector>

#include "Element.hpp"

#include "PieceComposee.hpp"

using namespace std;

class Assembleur : public Element
{
    private:
        vector<Element*> _previousActiveElements;
        PieceComposee* _piece;

    public:
        Assembleur(string name, double dureeTraitement);
        void addPreviousActiveElement(Element* activeElement);
        virtual bool isReady();
        virtual void insert(double dateCourante);
        virtual Piece* removePiece(double dateCourante);
};

#endif