#ifndef ACTIVE_ELEMENT_HPP
#define ACTIVE_ELEMENT_HPP


#include <iostream>
#include <limits>
#include <queue>
#include <random>
#include <tuple>
#include <vector>

#include "Piece.hpp"

using namespace std;

enum State {Working, Waiting, Blocked};

class Element
{
    private:
        vector<tuple<Element*, double>> _nextElements;
        string _name;
        double _dpe;
        double _dureeTraitement;
        State _state;
        queue<Element*> _waitingElements;

    public:
        Element(string name, double dpe, double dureeTraitement, State state);
        
        virtual void display();
        virtual bool isReady() = 0;
        virtual void insert(double date) = 0;
        virtual Piece* removePiece(double dateCourante) = 0;
        
        string getName();
        double getDpe();
        double getDureeTraitement();
        State getState();
        void setDpe(double dpe);
        void setState(State state);

        void addNextElement(Element* e, double p);
        Element* selectPreviousElement();
        Element* selectNextElement();
        void addWaitingElement(Element* m);

};

#endif